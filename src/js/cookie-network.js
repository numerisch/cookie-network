/*
Numerisch GmbH - Cookie Network
 */

var storage = new CrossStorageClient('http://localhost:3000/hub.html');
storage.onConnect()
.then(function() {
	return storage.get('cookies_accepted');
}).then(function(res) {
	if(res === "yes") {
		console.log("Cookies already accepted");
	} else {
		console.log("Cookies are not accepted yet");
		showCookieDialog();
	}
})['catch'](function(err) {
	console.log(err);
});


function showCookieDialog() {
	var cookieAcceptBar = document.createElement("DIV");
	cookieAcceptBar.id = "cookie-alert";

	var btn      = document.createElement("BUTTON");
	var span     = document.createElement("SPAN");
	var btnText  = document.createTextNode("O.K.");
	var spanText = document.createTextNode("Accept Cookies!");

	btn.addEventListener('click', function() {
		storage.set('cookies_accepted', 'yes').then(function() {
			cookieAcceptBar.parentNode.removeChild(cookieAcceptBar);
		});
	}, false);

	btn.appendChild(btnText);
	span.appendChild(spanText);
	cookieAcceptBar.appendChild(span);
	cookieAcceptBar.appendChild(btn);
	document.body.appendChild(cookieAcceptBar);
}