/*
npm install --save-dev
 */

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    concat: {
      client_js: {
        src: [
          "bower_components/cross-storage/dist/client.js",
          "src/js/cookie-network.js",
        ],
        dest: "web/js/cookie-network-client.js"
      },
    },
    uglify: {
      options: {
        mangle: false,
        banner: "/*! <%= pkg.name %> | <%= pkg.description %> */\n",
      },
      cookie_network: {
        files: {
          "web/js/cookie-network-client.min.js": ["web/js/cookie-network-client.js"],
          "web/js/cookie-network-hub.min.js": ["bower_components/cross-storage/dist/hub.js"],
        }
      },
    },
    cssmin: {
      minify: {
        expand: true,
        cwd: "web/css",
        src: ["*.css", "!*.min.css"],
        dest: "web/css",
        ext: ".min.css",
        options: {
          banner: "/*! <%= pkg.description %> */",
          keepSpecialComments: 0,
          report: "min"
        }
      }
    },
    less: {
      cookie_network: {
        files: {
          "web/css/cookie-network.css": "src/less/cookie-network.less"
        }
      }
    },
    watch: {
      options: {
          livereload: true,
      },
      less: {
        options: {
          livereload: true
        },
        files: "src/less/*",
        tasks: ["less:cookie_network", "cssmin:minify", "builtjs"]
      },
      scripts: {
        files: ["src/**/*.js", "!**/node_modules/**"],
        tasks: ["builtjs"]
      },
    }
  });
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-uglify");


  grunt.registerTask("default", "watch");

  grunt.registerTask("builtjs", [
    "concat:client_js",
    "uglify:cookie_network"
  ]);
  
  grunt.registerTask("dist", [
    "builtjs",             // concat and minify js
    "less:cookie_network", // cookie_network less to css
    "cssmin:minify",       // minify css
  ]);
};